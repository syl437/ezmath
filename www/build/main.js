webpackJsonp([14],{

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = Object.freeze({
    "SERVER_URL": 'http://www.tapper.org.il/ezmath/laravel/public/api/',
    "IMAGE_URL": 'http://www.tapper.org.il/ezmath/laravel/storage/app/public/',
    "FACEBOOKID": '1759132254144414',
    "TOAST": {
        "duration": 3000,
        "position": "buttom"
    },
    "FIREBASE_CONFIG": {
        "apiKey": "AIzaSyCYOVrRscQ26G5lAmOSfwrBFncNidaCSOE",
        "authDomain": "ionic3-blue-light.firebaseapp.com",
        "databaseURL": "https://ionic3-blue-light.firebaseio.com",
        "projectId": "ionic3-blue-light",
        "storageBucket": "ionic3-blue-light.appspot.com",
        "messagingSenderId": "519928359775"
    },
    "MAP_KEY": {
        "apiKey": 'AIzaSyA4-GoZzOqYTvxMe52YQZch5JaCFN6ACLg'
    }
});
//# sourceMappingURL=app-settings.js.map

/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClasssignupmodalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_server_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_toast_service__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ClasssignupmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClasssignupmodalPage = /** @class */ (function () {
    function ClasssignupmodalPage(navCtrl, navParams, viewCtrl, server, Toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.server = server;
        this.Toast = Toast;
        this.data = this.navParams.get('details');
        console.log("data:", this.data);
    }
    ClasssignupmodalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ClasssignupmodalPage');
    };
    ClasssignupmodalPage.prototype.confirmSignup = function () {
        var _this = this;
        this.server.registerUserToClass('ConfirmregisterUserToClass', localStorage.getItem("userid"), this.data.id).then(function (data) {
            console.log("ConfirmregisterUserToClass : ", data.json());
            var response = data.json();
            if (response.max_quan_reached == 0) {
                _this.data.already_signup = 1;
                _this.closeModal();
                _this.Toast.presentToast('הרשמה לשיעור בוצעה בהצלחה');
            }
            else {
                _this.Toast.presentToast('לא ניתן להרשם לשיעור זה, מכסת תלמידים לשיעור הסתיים');
            }
        });
    };
    ClasssignupmodalPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss(this.data);
    };
    ClasssignupmodalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-classsignupmodal',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\classsignupmodal\classsignupmodal.html"*/'\n\n<ion-content padding>\n\n    <div class="mainLogo" align="center">\n\n        <img src="images/logo.png" style="width:50%; margin: auto">\n\n    </div>\n\n\n\n    <div class="greenLine"></div>\n\n\n\n    <div class="mainTitle" >\n\n        <img src="images/confirm.png" width="100%"/>\n\n    </div>\n\n\n\n<div align="center" style="direction: rtl;">\n\n\n\n\n\n\n\n  <ion-grid>\n\n\n\n    <ion-row>\n\n      <ion-col col-md-6>\n\n        סניף:\n\n      </ion-col>\n\n      <ion-col col-md-6>\n\n       {{data.Branch}}\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col col-md-6>\n\n        תאריך:\n\n      </ion-col>\n\n      <ion-col col-md-6>\n\n        {{data.class_date}}\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col col-md-6>\n\n        מקצוע:\n\n      </ion-col>\n\n      <ion-col col-md-6>\n\n        {{data.Profession}}\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col col-md-6>\n\n        שעה:\n\n      </ion-col>\n\n      <ion-col col-md-6>\n\n        {{data.class_hour}}\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n\n\n    <ion-row>\n\n      <ion-col col-md-6>\n\n       כמות שעות:\n\n      </ion-col>\n\n      <ion-col col-md-6>\n\n        {{data.class_total_hours}}\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n  <ion-row>\n\n    <ion-col col-md-12>\n\n      <button ion-button full class="regButton" (click)="confirmSignup()">אישור</button>\n\n      <button ion-button full class="cancelButton" (click)="closeModal()">ביטול</button>\n\n    </ion-col>\n\n  </ion-row>\n\n\n\n  </ion-grid>\n\n\n\n\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\classsignupmodal\classsignupmodal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_server_service__["a" /* ServerService */], __WEBPACK_IMPORTED_MODULE_3__services_toast_service__["a" /* ToastService */]])
    ], ClasssignupmodalPage);
    return ClasssignupmodalPage;
}());

//# sourceMappingURL=classsignupmodal.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_server_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_toast_service__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ContactModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ContactModalPage = /** @class */ (function () {
    function ContactModalPage(navCtrl, navParams, server, Toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.server = server;
        this.Toast = Toast;
        this.info = {
            fullname: '',
            phone: '',
            description: '',
        };
    }
    ContactModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactModalPage');
    };
    ContactModalPage.prototype.sendContact = function () {
        var _this = this;
        if (!this.info.fullname)
            this.Toast.presentToast('הכנס שם מלא');
        else if (!this.info.phone)
            this.Toast.presentToast('הכנס טלפון');
        else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
            this.Toast.presentToast('הכנס מספר טלפון חוקי');
        else if (!this.info.description)
            this.Toast.presentToast('הכנס תיאור הפנייה');
        else {
            this.server.ContactSignup('ContactSignup', this.info, localStorage.getItem("userid")).then(function (data) {
                _this.info.fullname = '';
                _this.info.phone = '';
                _this.info.description = '';
                _this.closeModal();
            });
        }
    };
    ContactModalPage.prototype.closeModal = function () {
        this.navCtrl.pop();
    };
    ContactModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact-modal',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\contact-modal\contact-modal.html"*/'<!--\n<ion-header>\n  <ion-navbar>\n    <ion-title>צור קשר</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="closeModal()">\n        <ion-icon item-right name="ios-close-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n-->\n\n<ion-content padding>\n  <div class="mainLogo" align="center">\n    <img src="images/logo.png" style="width:50%; margin: auto">\n  </div>\n\n  <div class="greenLine"></div>\n\n\n\n  <div align="center" style="direction: rtl;">\n\n    <div class="form-group">\n      <input type="text" class="form-control textWhite" [(ngModel)]="info.fullname"   required  placeholder="הכנס שם מלא"><br>\n    </div>\n\n    <div class="form-group">\n      <input type="tel" class="form-control textWhite"  [(ngModel)]="info.phone"   required  placeholder="הכנס טלפון"><br>\n    </div>\n\n    <div class="form-group">\n      <textarea rows="4" cols="50" class="form-control textWhite" [(ngModel)]="info.description"   required  placeholder="תיאור הפנייה"></textarea>\n    </div>\n\n\n\n    <button ion-button full class="regButton" (click)="sendContact()">אישור</button>\n    <button ion-button full class="cancelButton" (click)="closeModal()">ביטול</button>\n\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\contact-modal\contact-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_server_service__["a" /* ServerService */], __WEBPACK_IMPORTED_MODULE_3__services_toast_service__["a" /* ToastService */]])
    ], ContactModalPage);
    return ContactModalPage;
}());

//# sourceMappingURL=contact-modal.js.map

/***/ }),

/***/ 208:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 208;

/***/ }),

/***/ 251:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/classsignup/classsignup.module": [
		825,
		13
	],
	"../pages/classsignupmodal/classsignupmodal.module": [
		826,
		12
	],
	"../pages/contact-modal/contact-modal.module": [
		827,
		11
	],
	"../pages/futureclasses/futureclasses.module": [
		829,
		8
	],
	"../pages/home/home.module": [
		828,
		7
	],
	"../pages/login/login.module": [
		830,
		6
	],
	"../pages/main/main.module": [
		831,
		5
	],
	"../pages/pastclasses/pastclasses.module": [
		832,
		4
	],
	"../pages/previoustickets/previoustickets.module": [
		833,
		10
	],
	"../pages/profileedit/profileedit.module": [
		834,
		3
	],
	"../pages/pushnotifications/pushnotifications.module": [
		836,
		9
	],
	"../pages/register-intro/register-intro.module": [
		835,
		2
	],
	"../pages/register/register.module": [
		838,
		1
	],
	"../pages/teacherconfirmclass/teacherconfirmclass.module": [
		837,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 251;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__footer_footer__ = __webpack_require__(796);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__facebookconnect_facebookconnect__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__back_button_back_button__ = __webpack_require__(804);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__footer_footer__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_3__facebookconnect_facebookconnect__["a" /* FacebookconnectComponent */],
                __WEBPACK_IMPORTED_MODULE_4__back_button_back_button__["a" /* BackButtonComponent */],
            ],
            imports: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_1__footer_footer__["a" /* FooterComponent */])],
            exports: [__WEBPACK_IMPORTED_MODULE_1__footer_footer__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_3__facebookconnect_facebookconnect__["a" /* FacebookconnectComponent */],
                __WEBPACK_IMPORTED_MODULE_4__back_button_back_button__["a" /* BackButtonComponent */],
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClasssignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_server_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_toast_service__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__classsignupmodal_classsignupmodal__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__contact_modal_contact_modal__ = __webpack_require__(175);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the ClasssignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClasssignupPage = /** @class */ (function () {
    function ClasssignupPage(navCtrl, navParams, server, Toast, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.server = server;
        this.Toast = Toast;
        this.modalCtrl = modalCtrl;
        this.todaydate = __WEBPACK_IMPORTED_MODULE_5_moment___default()().format('YYYY-MM-DD');
        this.Classrow = [];
        this.info = {
            'branchselect': '',
            'professionselect': '',
            'class_date': this.todaydate,
            'class_end_date': this.todaydate
        };
        this.searchArray = [];
    }
    ClasssignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ClasssignupPage');
    };
    ClasssignupPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getBranches();
                this.GetProfessions();
                return [2 /*return*/];
            });
        });
    };
    ClasssignupPage.prototype.getBranches = function () {
        var _this = this;
        this.server.GetBranches('GetBranches').then(function (data) {
            console.log("GetBranches : ", data.json());
            _this.brancesArray = data.json();
        });
    };
    ClasssignupPage.prototype.GetProfessions = function () {
        var _this = this;
        this.server.GetBranches('GetProfessions').then(function (data) {
            console.log("GetProfessions : ", data.json());
            _this.professionArray = data.json();
        });
    };
    ClasssignupPage.prototype.searchClass = function () {
        var _this = this;
        if (!this.info.professionselect)
            this.Toast.presentToast('יש לבחור מקצוע');
        else if (!this.info.branchselect)
            this.Toast.presentToast('יש לבחור סניף');
        else if (!this.info.class_date)
            this.Toast.presentToast('יש לבחור תאריך');
        else if (!this.info.class_end_date)
            this.Toast.presentToast('יש לבחור תאריך סיום');
        else {
            this.server.SearchClasses('SearchClasses', this.info, localStorage.getItem("userid")).then(function (data) {
                console.log("SearchClasses : ", data.json());
                _this.searchArray = data.json();
                if (data.json() == 0)
                    _this.Toast.presentToast('לא נמצאו תוצאות');
            });
        }
    };
    ClasssignupPage.prototype.registerUserToClass = function (row) {
        var _this = this;
        this.Classrow = row;
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        if (hours < 10)
            hours = "0" + hours;
        if (minutes < 10)
            minutes = "0" + minutes;
        var time = hours + ':' + minutes;
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if (month < 10)
            month = '0' + month.toString();
        if (day < 10)
            day = '0' + day.toString();
        var todayDate = year + '-' + month + '-' + day;
        if (row.already_signup == 1)
            this.Toast.presentToast('הינך כבר רשום לשיעור זה');
        else {
            if (row.old_date < todayDate) {
                this.Toast.presentToast('לא ניתן להרשם לשיעור בתאריך קטן מהיום');
                return;
            }
            else {
                if (row.old_date == todayDate) {
                    if (time > row.class_hour) {
                        this.Toast.presentToast('לא ניתן להרשם לשיעור שעת הרשמה לשיעור זה הסתיימה');
                        return;
                    }
                }
            }
            this.server.registerUserToClass('registerUserToClass', localStorage.getItem("userid"), row.id).then(function (data) {
                console.log("registerUserToClass : ", data.json());
                var response = data.json();
                if (response.max_quan_reached == 0) {
                    //row.already_signup = 1;
                    var myModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__classsignupmodal_classsignupmodal__["a" /* ClasssignupmodalPage */], { details: row });
                    myModal.present();
                }
                else {
                    _this.Toast.presentToast('לא ניתן להרשם לשיעור זה, מכסת תלמידים לשיעור הסתיים');
                }
            });
        }
    };
    ClasssignupPage.prototype.OpenContactModal = function () {
        var ContactModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__contact_modal_contact_modal__["a" /* ContactModalPage */]);
        ContactModal.present();
    };
    ClasssignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-classsignup',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\classsignup\classsignup.html"*/'<!--\n\n  Generated template for the ClasssignupPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n\n\n\n\n\n\n\n\n\n\n<ion-content padding>\n\n    <back-button></back-button>\n\n\n\n    <div class="topRightCorner" (click)="OpenContactModal()">\n\n        <ion-icon name="ios-information-circle-outline"></ion-icon>\n\n    </div>\n\n    <div class="mainLogo" align="center">\n\n        <img src="images/logo.png" style="width:50%; margin: auto">\n\n    </div>\n\n    <div class="mainTitle" >\n\n        <img src="images/subscribe.png" width="100%"/>\n\n    </div>\n\n    <div class="selectInfo">\n\n        <select class="form-control" [(ngModel)]="info.professionselect">\n\n            <option value="">בחירת מקצוע</option>\n\n            <option [value]="row.id" *ngFor="let row of professionArray">{{row.title}}</option>\n\n        </select>\n\n\n\n        <select class="form-control" [(ngModel)]="info.branchselect">\n\n            <option value="">בחירת סניף</option>\n\n            <option [value]="row.id" *ngFor="let row of brancesArray">{{row.title}}</option>\n\n        </select>\n\n\n\n\n\n        <ion-grid>\n\n            <ion-row justify-content-start>\n\n\n\n                <ion-col col-6 col-sm>\n\n                    <div class="form-group" class="col-lg-6"  style="margin-top: 15px;">\n\n                        <div style="color: #ffffff; text-align:right; direction: rtl;" >עד: </div>\n\n                        <input type="date" [min]="todaydate" class="form-control" [(ngModel)]="info.class_end_date">\n\n                    </div>\n\n                </ion-col>\n\n\n\n                <ion-col col-6 col-sm>\n\n                    <div class="form-group" class="col-lg-6"  style="margin-top: 15px;">\n\n                        <div style="color: #ffffff; text-align:right; direction: rtl;" >מ: </div>\n\n                        <input type="date" [min]="todaydate" class="form-control" [(ngModel)]="info.class_date">\n\n                    </div>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n        </ion-grid>\n\n\n\n\n\n\n\n\n\n        <button ion-button full class="regButton" (click)="searchClass()" >חיפוש</button>\n\n    </div>\n\n\n\n\n\n    <div *ngIf="searchArray.length > 0" class="MT20">\n\n\n\n        <div class="mainRow1" *ngFor="let row of searchArray let i=index">\n\n            <div class="rowDiv">\n\n                <div class="Title1">{{row.title}}</div>\n\n                <div class="Title1">{{row.class_date}} | {{row.class_hour}}</div>\n\n\n\n                <div class="Date1">\n\n                    <button ion-button small type="button" (click)="registerUserToClass(row)" [ngStyle]="{\n\n                \'background-color\':row.already_signup == \'0\' && row.max_quan_reached == 0 ? \'green\' : \'#ccc\'}">\n\n                        <span *ngIf="row.already_signup == 0 && row.max_quan_reached == 0">הרשמה</span>\n\n                        <span *ngIf="row.already_signup == 1">רשום</span>\n\n                        <span *ngIf="row.max_quan_reached == 1 && row.already_signup == 0">מכסה</span>\n\n                    </button>\n\n                </div>\n\n            </div>\n\n        </div>\n\n\n\n        <!--\n\n        <div class="Lessons" *ngFor="let row of searchArray let i=index">\n\n            <div class="Status" >\n\n\n\n                <button ion-button small type="button" (click)="registerUserToClass(row)" [ngStyle]="{\n\n                \'background-color\':row.already_signup == \'0\' && row.max_quan_reached == 0 ? \'green\' : \'#ccc\'}">\n\n                    <span *ngIf="row.already_signup == 0 && row.max_quan_reached == 0">הרשמה</span>\n\n                    <span *ngIf="row.already_signup == 1">רשום</span>\n\n                    <span *ngIf="row.max_quan_reached == 1 && row.already_signup == 0">מכסה</span>\n\n                </button>\n\n            </div>\n\n            <div class="Time">\n\n                <button ion-button small color="primary" type="button"> {{row.class_hour}}</button>\n\n            </div>\n\n            <div class="Date">\n\n                <button ion-button small color="primary" type="button"> {{row.class_date}}</button>\n\n            </div>\n\n            <div class="Title">\n\n                <button ion-button small color="primary" style="" type="button"> {{row.title}}</button>\n\n            </div>\n\n        </div>\n\n\n\n        -->\n\n    </div>\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\classsignup\classsignup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_server_service__["a" /* ServerService */],
            __WEBPACK_IMPORTED_MODULE_3__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
    ], ClasssignupPage);
    return ClasssignupPage;
}());

//# sourceMappingURL=classsignup.js.map

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreviousticketsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_server_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



/**
 * Generated class for the PreviousticketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PreviousticketsPage = /** @class */ (function () {
    function PreviousticketsPage(navCtrl, navParams, server) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.server = server;
        this.Type = 1;
        this.userTicketsArray = [];
        this.paymentsArray = [];
        this.paymentMethod = ["מזומן", "כרטיסיה", "חוב"];
    }
    PreviousticketsPage.prototype.changeType = function (type) {
        this.Type = type;
    };
    PreviousticketsPage.prototype.getTickets = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.server.GetpreviousTickets('GetpreviousTickets', localStorage.getItem("userid")).then(function (data) {
                            console.log("GetpreviousTickets : ", data.json());
                            var response = data.json();
                            _this.userTicketsArray = response.tickets;
                            _this.paymentsArray = response.payments;
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PreviousticketsPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getTickets();
                return [2 /*return*/];
            });
        });
    };
    PreviousticketsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PreviousticketsPage');
    };
    PreviousticketsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-previoustickets',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\previoustickets\previoustickets.html"*/'<!--<ion-header>-->\n\n<!--<ion-navbar>-->\n<!--<ion-title>פרטי תלמיד</ion-title>-->\n<!--</ion-navbar>-->\n\n<!--</ion-header>-->\n\n<ion-content>\n  <back-button></back-button>\n\n  <div class="mainLogo" align="center">\n    <img src="images/logo.png" style="width:50%; margin: auto">\n  </div>\n\n  <div class="Types" >\n    <div class="typeRight" (click)="changeType(2)">\n      <button ion-button [ngClass]="{\'buttonColor1\' : (Type == 2), \'buttonColor2\':(Type == 1)}">חיובים</button>\n    </div>\n    <div class="typeLeft" (click)="changeType(1)">\n      <button ion-button   [ngClass]="{\'buttonColor1\' : (Type == 1), \'buttonColor2\':(Type == 2)}" >כרטיסיות</button>\n    </div>\n  </div>\n\n  <div id="info" align="center">\n\n    <div *ngIf="Type == 1">\n\n      <div class="MT20" *ngIf="userTicketsArray.length == 0">\n        לא נמצאו כרטיסיות\n      </div>\n\n      <div class="mainRow1" *ngFor="let row of userTicketsArray let i=index">\n        <div class="rowDiv">\n          <div class="Title1">תאריך כרטיסיה :  {{row.ticket_date}}</div>\n        </div>\n        <div class="rowDiv">\n          <div class="Status1">12 ניקובים</div>\n          <div class="Hour1"> {{row.created_at}} </div>\n        </div>\n      </div>\n\n    </div>\n\n    <div *ngIf="Type == 2">\n\n      <div class="MT20" *ngIf="paymentsArray.length == 0">\n        לא נמצאו חיובים\n      </div>\n\n      <div class="mainRow1" *ngFor="let row of paymentsArray let i=index">\n        <div class="rowDiv">\n          <div class="Title1" *ngIf="row.class">  {{row.class[0].title}} | {{row.class[0].profession}} | {{row.class[0].branch}}</div>\n          <div class="Title1" *ngIf="row.class">  {{row.class[0].class_total_hours}} שעות</div>\n          <div class="Title1">  {{paymentMethod[row.payment_method]}} </div>\n        </div>\n        <div class="rowDiv">\n          <div class="Hour1">  {{row.created_at}} </div>\n        </div>\n      </div>\n\n    </div>\n\n\n  </div>\n\n\n\n\n</ion-content>\n\n<ion-footer>\n  <footer></footer>\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\previoustickets\previoustickets.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_server_service__["a" /* ServerService */]])
    ], PreviousticketsPage);
    return PreviousticketsPage;
}());

//# sourceMappingURL=previoustickets.js.map

/***/ }),

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(537);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_settings__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(150);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ServerService = /** @class */ (function () {
    function ServerService(loadingCtrl, storage, http) {
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.http = http;
    }
    ServerService.prototype.GetBranches = function (url) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.GetProfessions = function (url) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.SearchClasses = function (url, data, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.ClassPayment = function (url, data, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.userDidntShow = function (url, data, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.registerUserToClass = function (url, user_id, class_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            body.append("class_id", JSON.stringify(class_id));
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.getStudentData = function (url, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.saveUserProfile = function (url, data, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.getFutureClasses = function (url, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.cancelFutureClass = function (url, class_id, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            body.append("class_id", class_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.getPastClasses = function (url, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.GetHomePageData = function (url, user_id) {
        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
        //loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            //loading.dismiss();
        }
    };
    ServerService.prototype.GetpreviousTickets = function (url, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.ContactSignup = function (url, data, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService.prototype.GetData = function (url) {
        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
        //loading.present();
        try {
            var body = new FormData();
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            // loading.dismiss();
        }
    };
    ServerService.prototype.getPushNotifications = function (url, user_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    ServerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */]])
    ], ServerService);
    return ServerService;
}());

//this._categories.next(categories);
//resolve(this.categories);
//# sourceMappingURL=server-service.js.map

/***/ }),

/***/ 537:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(822);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_auth_service__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_server_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_toast_service__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_facebook__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_firebase__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_components_module__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_classsignupmodal_classsignupmodal__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_contact_modal_contact_modal__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_previoustickets_previoustickets__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_classsignup_classsignup__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_network__ = __webpack_require__(823);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_pushnotifications_pushnotifications__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















//import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_classsignupmodal_classsignupmodal__["a" /* ClasssignupmodalPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_contact_modal_contact_modal__["a" /* ContactModalPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_classsignup_classsignup__["a" /* ClasssignupPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_pushnotifications_pushnotifications__["a" /* PushnotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_previoustickets_previoustickets__["a" /* PreviousticketsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/classsignup/classsignup.module#ClasssignupPageModule', name: 'ClasssignupPage', segment: 'classsignup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/classsignupmodal/classsignupmodal.module#ClasssignupmodalPageModule', name: 'ClasssignupmodalPage', segment: 'classsignupmodal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-modal/contact-modal.module#ContactModalPageModule', name: 'ContactModalPage', segment: 'contact-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/futureclasses/futureclasses.module#FutureclassesPageModule', name: 'FutureclassesPage', segment: 'futureclasses', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/main.module#MainPageModule', name: 'MainPage', segment: 'main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pastclasses/pastclasses.module#PastclassesPageModule', name: 'PastclassesPage', segment: 'pastclasses', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/previoustickets/previoustickets.module#PreviousticketsPageModule', name: 'PreviousticketsPage', segment: 'previoustickets', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profileedit/profileedit.module#ProfileeditPageModule', name: 'ProfileeditPage', segment: 'profileedit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register-intro/register-intro.module#RegisterIntroPageModule', name: 'RegisterIntroPage', segment: 'register-intro', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pushnotifications/pushnotifications.module#PushnotificationsPageModule', name: 'PushnotificationsPage', segment: 'pushnotifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/teacherconfirmclass/teacherconfirmclass.module#TeacherconfirmclassPageModule', name: 'TeacherconfirmclassPage', segment: 'teacherconfirmclass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_13__components_components_module__["a" /* ComponentsModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_classsignupmodal_classsignupmodal__["a" /* ClasssignupmodalPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_contact_modal_contact_modal__["a" /* ContactModalPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_classsignup_classsignup__["a" /* ClasssignupPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_pushnotifications_pushnotifications__["a" /* PushnotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_previoustickets_previoustickets__["a" /* PreviousticketsPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_7__services_server_service__["a" /* ServerService */],
                __WEBPACK_IMPORTED_MODULE_10__services_toast_service__["a" /* ToastService */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_firebase__["a" /* Firebase */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_network__["a" /* Network */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_settings__ = __webpack_require__(145);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ToastService = /** @class */ (function () {
    function ToastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ToastService.prototype.presentToast = function (message) {
        console.log("Toast : ", __WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].TOAST);
        var toastItem = __WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].TOAST;
        toastItem["message"] = message;
        toastItem["cssClass"] = "mainToastClass";
        var toast = this.toastCtrl.create(toastItem);
        toast.present();
    };
    ToastService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* ToastController */]])
    ], ToastService);
    return ToastService;
}());

//# sourceMappingURL=toast-service.js.map

/***/ }),

/***/ 795:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 261,
	"./af.js": 261,
	"./ar": 262,
	"./ar-dz": 263,
	"./ar-dz.js": 263,
	"./ar-kw": 264,
	"./ar-kw.js": 264,
	"./ar-ly": 265,
	"./ar-ly.js": 265,
	"./ar-ma": 266,
	"./ar-ma.js": 266,
	"./ar-sa": 267,
	"./ar-sa.js": 267,
	"./ar-tn": 268,
	"./ar-tn.js": 268,
	"./ar.js": 262,
	"./az": 269,
	"./az.js": 269,
	"./be": 270,
	"./be.js": 270,
	"./bg": 271,
	"./bg.js": 271,
	"./bm": 272,
	"./bm.js": 272,
	"./bn": 273,
	"./bn.js": 273,
	"./bo": 274,
	"./bo.js": 274,
	"./br": 275,
	"./br.js": 275,
	"./bs": 276,
	"./bs.js": 276,
	"./ca": 277,
	"./ca.js": 277,
	"./cs": 278,
	"./cs.js": 278,
	"./cv": 279,
	"./cv.js": 279,
	"./cy": 280,
	"./cy.js": 280,
	"./da": 281,
	"./da.js": 281,
	"./de": 282,
	"./de-at": 283,
	"./de-at.js": 283,
	"./de-ch": 284,
	"./de-ch.js": 284,
	"./de.js": 282,
	"./dv": 285,
	"./dv.js": 285,
	"./el": 286,
	"./el.js": 286,
	"./en-au": 287,
	"./en-au.js": 287,
	"./en-ca": 288,
	"./en-ca.js": 288,
	"./en-gb": 289,
	"./en-gb.js": 289,
	"./en-ie": 290,
	"./en-ie.js": 290,
	"./en-il": 291,
	"./en-il.js": 291,
	"./en-nz": 292,
	"./en-nz.js": 292,
	"./eo": 293,
	"./eo.js": 293,
	"./es": 294,
	"./es-do": 295,
	"./es-do.js": 295,
	"./es-us": 296,
	"./es-us.js": 296,
	"./es.js": 294,
	"./et": 297,
	"./et.js": 297,
	"./eu": 298,
	"./eu.js": 298,
	"./fa": 299,
	"./fa.js": 299,
	"./fi": 300,
	"./fi.js": 300,
	"./fo": 301,
	"./fo.js": 301,
	"./fr": 302,
	"./fr-ca": 303,
	"./fr-ca.js": 303,
	"./fr-ch": 304,
	"./fr-ch.js": 304,
	"./fr.js": 302,
	"./fy": 305,
	"./fy.js": 305,
	"./gd": 306,
	"./gd.js": 306,
	"./gl": 307,
	"./gl.js": 307,
	"./gom-latn": 308,
	"./gom-latn.js": 308,
	"./gu": 309,
	"./gu.js": 309,
	"./he": 310,
	"./he.js": 310,
	"./hi": 311,
	"./hi.js": 311,
	"./hr": 312,
	"./hr.js": 312,
	"./hu": 313,
	"./hu.js": 313,
	"./hy-am": 314,
	"./hy-am.js": 314,
	"./id": 315,
	"./id.js": 315,
	"./is": 316,
	"./is.js": 316,
	"./it": 317,
	"./it.js": 317,
	"./ja": 318,
	"./ja.js": 318,
	"./jv": 319,
	"./jv.js": 319,
	"./ka": 320,
	"./ka.js": 320,
	"./kk": 321,
	"./kk.js": 321,
	"./km": 322,
	"./km.js": 322,
	"./kn": 323,
	"./kn.js": 323,
	"./ko": 324,
	"./ko.js": 324,
	"./ky": 325,
	"./ky.js": 325,
	"./lb": 326,
	"./lb.js": 326,
	"./lo": 327,
	"./lo.js": 327,
	"./lt": 328,
	"./lt.js": 328,
	"./lv": 329,
	"./lv.js": 329,
	"./me": 330,
	"./me.js": 330,
	"./mi": 331,
	"./mi.js": 331,
	"./mk": 332,
	"./mk.js": 332,
	"./ml": 333,
	"./ml.js": 333,
	"./mn": 334,
	"./mn.js": 334,
	"./mr": 335,
	"./mr.js": 335,
	"./ms": 336,
	"./ms-my": 337,
	"./ms-my.js": 337,
	"./ms.js": 336,
	"./mt": 338,
	"./mt.js": 338,
	"./my": 339,
	"./my.js": 339,
	"./nb": 340,
	"./nb.js": 340,
	"./ne": 341,
	"./ne.js": 341,
	"./nl": 342,
	"./nl-be": 343,
	"./nl-be.js": 343,
	"./nl.js": 342,
	"./nn": 344,
	"./nn.js": 344,
	"./pa-in": 345,
	"./pa-in.js": 345,
	"./pl": 346,
	"./pl.js": 346,
	"./pt": 347,
	"./pt-br": 348,
	"./pt-br.js": 348,
	"./pt.js": 347,
	"./ro": 349,
	"./ro.js": 349,
	"./ru": 350,
	"./ru.js": 350,
	"./sd": 351,
	"./sd.js": 351,
	"./se": 352,
	"./se.js": 352,
	"./si": 353,
	"./si.js": 353,
	"./sk": 354,
	"./sk.js": 354,
	"./sl": 355,
	"./sl.js": 355,
	"./sq": 356,
	"./sq.js": 356,
	"./sr": 357,
	"./sr-cyrl": 358,
	"./sr-cyrl.js": 358,
	"./sr.js": 357,
	"./ss": 359,
	"./ss.js": 359,
	"./sv": 360,
	"./sv.js": 360,
	"./sw": 361,
	"./sw.js": 361,
	"./ta": 362,
	"./ta.js": 362,
	"./te": 363,
	"./te.js": 363,
	"./tet": 364,
	"./tet.js": 364,
	"./tg": 365,
	"./tg.js": 365,
	"./th": 366,
	"./th.js": 366,
	"./tl-ph": 367,
	"./tl-ph.js": 367,
	"./tlh": 368,
	"./tlh.js": 368,
	"./tr": 369,
	"./tr.js": 369,
	"./tzl": 370,
	"./tzl.js": 370,
	"./tzm": 371,
	"./tzm-latn": 372,
	"./tzm-latn.js": 372,
	"./tzm.js": 371,
	"./ug-cn": 373,
	"./ug-cn.js": 373,
	"./uk": 374,
	"./uk.js": 374,
	"./ur": 375,
	"./ur.js": 375,
	"./uz": 376,
	"./uz-latn": 377,
	"./uz-latn.js": 377,
	"./uz.js": 376,
	"./vi": 378,
	"./vi.js": 378,
	"./x-pseudo": 379,
	"./x-pseudo.js": 379,
	"./yo": 380,
	"./yo.js": 380,
	"./zh-cn": 381,
	"./zh-cn.js": 381,
	"./zh-hk": 382,
	"./zh-hk.js": 382,
	"./zh-tw": 383,
	"./zh-tw.js": 383
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 795;

/***/ }),

/***/ 796:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_pushnotifications_pushnotifications__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FooterComponent = /** @class */ (function () {
    function FooterComponent(navCtrl) {
        this.navCtrl = navCtrl;
    }
    FooterComponent.prototype.goHomePage = function () {
        this.navCtrl.setRoot('MainPage');
    };
    FooterComponent.prototype.goPreviousClassesPage = function () {
        this.navCtrl.push('PastclassesPage');
    };
    FooterComponent.prototype.goProfilePage = function () {
        this.navCtrl.push('ProfileeditPage');
    };
    FooterComponent.prototype.goFutureClassesPage = function () {
        this.navCtrl.push('FutureclassesPage');
    };
    FooterComponent.prototype.goPushNotifications = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_pushnotifications_pushnotifications__["a" /* PushnotificationsPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FooterComponent.prototype, "messagesCount", void 0);
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'footer',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\components\footer\footer.html"*/'<!--<ion-toolbar no-padding>-->\n\n  <!--<ion-grid>-->\n\n    <!--<ion-row text-center>-->\n\n      <!--<ion-col (click)="goFutureClassesPage()">-->\n\n        <!--<div>-->\n\n          <!--<ion-icon name="ios-clock"></ion-icon>-->\n\n        <!--</div>-->\n\n        <!--<div ion-text color="primary">שיעורים עתידיים</div>-->\n\n      <!--</ion-col>-->\n\n      <!--<ion-col (click)="goProfilePage()">-->\n\n        <!--<div>-->\n\n          <!--<ion-icon name="ios-contact"></ion-icon>-->\n\n        <!--</div>-->\n\n        <!--<div ion-text color="primary">פרטי תלמיד</div>-->\n\n      <!--</ion-col>-->\n\n      <!--<ion-col (click)="goPreviousClassesPage()">-->\n\n        <!--<div>-->\n\n          <!--<ion-icon name="ios-rewind"></ion-icon>-->\n\n        <!--</div>-->\n\n        <!--<div ion-text color="primary">שיעורים שהיו</div>-->\n\n      <!--</ion-col>-->\n\n      <!--<ion-col (click)="goHomePage()">-->\n\n        <!--<div>-->\n\n          <!--<ion-icon name="md-home"></ion-icon>-->\n\n        <!--</div>-->\n\n        <!--<div ion-text color="primary">מסך הבית</div>-->\n\n      <!--</ion-col>-->\n\n    <!--</ion-row>-->\n\n  <!--</ion-grid>-->\n\n<!--</ion-toolbar>-->\n\n\n\n\n\n<table class="footerTable">\n\n    <tr>\n\n        <td (click)="goPushNotifications()">\n\n            <img src="images/home/ic6.png" style="width: 100%; position: relative;">\n\n            <ion-badge id="notifications-badge" color="danger" *ngIf="messagesCount > 0">{{messagesCount}}</ion-badge>\n\n        </td>\n\n        <td (click)="goFutureClassesPage()">\n\n            <img src="images/home/ic1.png" style="width: 100%">\n\n        </td>\n\n        <td (click)="goProfilePage()">\n\n            <img src="images/home/ic2.png" style="width: 100%">\n\n        </td>\n\n        <td (click)="goPreviousClassesPage()">\n\n            <img src="images/home/ic3.png" style="width: 100%">\n\n        </td>\n\n        <td (click)="goHomePage()">\n\n            <img src="images/home/ic4.png" style="width: 100%">\n\n        </td>\n\n    </tr>\n\n</table>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\components\footer\footer.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], FooterComponent);
    return FooterComponent;
}());

//# sourceMappingURL=footer.js.map

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacebookconnectComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_facebook__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toast_service__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the FacebookconnectComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FacebookconnectComponent = /** @class */ (function () {
    function FacebookconnectComponent(navCtrl, fb, auth, toast) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.auth = auth;
        this.toast = toast;
        this.isLoggedIn = false;
    }
    FacebookconnectComponent.prototype.faceBookLogin = function () {
        var _this = this;
        this.fb.login(['public_profile', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                _this.isLoggedIn = true;
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.isLoggedIn = false;
                //alert(JSON.stringify(res));
            }
        })
            .catch(function (e) {
            return console.log(e);
        });
    };
    FacebookconnectComponent.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            //alert(JSON.stringify(res));
            //alert (res['email']);
            _this.auth.faceBookLogin('faceBookLogin', res['email']).then(function (data) {
                console.log("faceBookLogin : ", data);
                var response = data.json();
                //alert(JSON.stringify(response));
                if (response.status == 0) {
                    _this.toast.presentToast("משתמש לא נמצא יש תחילה להרשם");
                    _this.navCtrl.push("RegisterPage");
                }
                else {
                    localStorage.setItem("userid", response.user_id);
                    localStorage.setItem("userType", response.userType);
                    _this.navCtrl.setRoot('MainPage');
                }
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    FacebookconnectComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'facebookconnect',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\components\facebookconnect\facebookconnect.html"*/'<!-- Generated template for the FacebookconnectComponent component -->\n\n<div  (click)="faceBookLogin()">\n\n    <img src="images/main_bt3.png" style="width: 100%" class="MT20" />\n\n</div>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\components\facebookconnect\facebookconnect.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_2__services_toast_service__["a" /* ToastService */]])
    ], FacebookconnectComponent);
    return FacebookconnectComponent;
}());

//# sourceMappingURL=facebookconnect.js.map

/***/ }),

/***/ 804:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackButtonComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BackButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var BackButtonComponent = /** @class */ (function () {
    function BackButtonComponent(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ;
    BackButtonComponent.prototype.backButtonClick = function () {
        this.navCtrl.pop();
    };
    BackButtonComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'back-button',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\components\back-button\back-button.html"*/'<!-- Generated template for the BackButtonComponent component -->\n\n<div>\n\n    <div class="headerLeftIcon" (click)="backButtonClick()">\n\n        <ion-icon name="ios-arrow-back" style="margin-left: 6px;"></ion-icon>\n\n        <div>חזור</div>\n\n    </div>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\components\back-button\back-button.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], BackButtonComponent);
    return BackButtonComponent;
}());

//# sourceMappingURL=back-button.js.map

/***/ }),

/***/ 822:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_firebase__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_auth_service__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_pushnotifications_pushnotifications__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, firebase, auth, toastCtrl) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.firebase = firebase;
        this.auth = auth;
        this.toastCtrl = toastCtrl;
        this.initializeApp();
        /*
        this.network.onDisconnect().subscribe( () => {
            if (this.previousStatus == "Online") {
                this.Toast.presentToast('יש לבדוק חיבור לאינטרנט');
            }
            this.previousStatus = "Offline";
        });
        Network.onConnect().subscribe(() => {
            if (this.previousStatus == "Offline") {
            }
            this.previousStatus = "Online";
        });
        */
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: 'HomePage' }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.platform.ready().then(function () {
                    // Okay, so the platform is ready and our plugins are available.
                    // Here you can do any higher level native things you might need.
                    _this.statusBar.styleDefault();
                    _this.splashScreen.hide();
                    var offline = __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__["Observable"].fromEvent(window, "offline");
                    var online = __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__["Observable"].fromEvent(window, "online");
                    offline.subscribe(function () {
                        var NoInternet = _this.toastCtrl.create({
                            message: 'יש לבדוק חיבור לאינטרנט',
                            showCloseButton: true,
                            closeButtonText: 'Ok'
                        });
                        NoInternet.present();
                        //this.Toast.presentToast('יש לבדוק חיבור לאינטרנט');
                    });
                    online.subscribe(function () {
                        console.log('Online event was detected.');
                    });
                    if (_this.platform.is('cordova')) {
                        if (_this.platform.is('android')) {
                            _this.initializeFireBaseAndroid();
                        }
                        if (_this.platform.is('ios')) {
                            _this.initializeFireBaseIos();
                        }
                    }
                    console.log(localStorage.getItem("id"));
                    if (localStorage.getItem("userid") != null && localStorage.getItem("userid") != '')
                        _this.nav.setRoot('MainPage');
                    else
                        _this.nav.setRoot('RegisterIntroPage');
                });
                return [2 /*return*/];
            });
        });
    };
    MyApp.prototype.initializeFireBaseAndroid = function () {
        var _this = this;
        return this.firebase.getToken()
            .catch(function (error) {
            return console.error('Error getting token', error);
        })
            .then(function (token) {
            _this.firebase.subscribe('all').then(function (result) {
                if (result)
                    console.log("Subscribed to all");
                _this.subscribeToPushNotificationEvents();
            });
        });
    };
    MyApp.prototype.initializeFireBaseIos = function () {
        var _this = this;
        return this.firebase.grantPermission()
            .catch(function (error) { return console.error('Error getting permission', error); })
            .then(function () {
            _this.firebase.getToken()
                .catch(function (error) { return console.error('Error getting token', error); })
                .then(function (token) {
                console.log("The token is " + token);
                _this.firebase.subscribe('all').then(function (result) {
                    if (result)
                        console.log("Subscribed to all");
                    _this.subscribeToPushNotificationEvents();
                });
            });
        });
    };
    MyApp.prototype.saveToken = function (token) {
        // Send the token to the server
        // console.log('Sending token to the server...');
        return Promise.resolve(true);
    };
    MyApp.prototype.subscribeToPushNotificationEvents = function () {
        var _this = this;
        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(function (token) {
            _this.auth.SetUserPush('SetUserPush', token, window.localStorage.userid);
            //this.saveToken(token);
            //this.loginService.sendToken('GetToken', token).then((data: any) => {
            //    console.log("UserDetails : ", data);
            //});
        }, function (error) {
            console.error('Error refreshing token', error);
        });
        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(function (notification) {
            // !notification.tap
            //     ? alert('The user was using the app when the notification arrived...')
            //     : alert('The app was closed when the notification arrived...');
            if (!notification.tap) {
                //alert(JSON.stringify(notification));
            }
            else {
                //alert(JSON.stringify(notification));
                if (notification.from_admin == "1")
                    _this.nav.push(__WEBPACK_IMPORTED_MODULE_7__pages_pushnotifications_pushnotifications__["a" /* PushnotificationsPage */]);
            }
        }, function (error) {
            console.error('Error getting the notification', error);
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_firebase__["a" /* Firebase */],
            __WEBPACK_IMPORTED_MODULE_5__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_settings__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(150);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthService = /** @class */ (function () {
    function AuthService(loadingCtrl, storage, http) {
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.http = http;
    }
    AuthService.prototype.UserLogin = function (url, data, push_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("push_id", push_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise(); // Reach here if fails
        }
        catch (err) {
            console.log("error11:", err);
        }
        finally {
            loading.dismiss();
        }
    };
    AuthService.prototype.registerUser = function (url, data, File, push_id) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("push_id", push_id);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    AuthService.prototype.faceBookLogin = function (url, data) {
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        try {
            var body = new FormData();
            body.append("category", data);
            return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
            }).toPromise();
        }
        catch (err) {
            console.log(err);
        }
        finally {
            loading.dismiss();
        }
    };
    AuthService.prototype.SetUserPush = function (url, push_token, user_id) {
        if (push_token)
            localStorage.setItem("push_id", push_token);
        if (user_id && push_token) {
            try {
                var body = new FormData();
                body.append("user_id", user_id);
                body.append("push_id", push_token);
                return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_settings__["a" /* AppSettings */].SERVER_URL + '' + url, body).map(function (res) { return res; }).do(function (data) {
                }).toPromise();
            }
            catch (err) {
                console.log(err);
            }
            finally {
            }
        }
    };
    AuthService.prototype.userLogOut = function (url, user_id) {
        localStorage.removeItem("userid");
        localStorage.removeItem("userType");
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */]])
    ], AuthService);
    return AuthService;
}());

//this._categories.next(categories);
//resolve(this.categories);
//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushnotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_server_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



/**
 * Generated class for the PushnotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PushnotificationsPage = /** @class */ (function () {
    function PushnotificationsPage(navCtrl, navParams, server) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.server = server;
        this.PushNotifications = [];
    }
    PushnotificationsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PushnotificationsPage');
    };
    PushnotificationsPage.prototype.getPushNotifications = function () {
        var _this = this;
        this.server.getPushNotifications('getPushNotifications', localStorage.getItem("userid")).then(function (data) {
            console.log("getPushNotifications : ", data);
            var response = data.json();
            _this.PushNotifications = response;
            console.log("getPushNotifications", _this.PushNotifications);
        });
    };
    PushnotificationsPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    PushnotificationsPage.prototype.ionViewWillEnter = function () {
        this.getPushNotifications();
    };
    PushnotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pushnotifications',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\pushnotifications\pushnotifications.html"*/'<!--\n  Generated template for the FutureclassesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--<ion-header>-->\n\n<!--<ion-navbar>-->\n<!--<ion-title>שיעורים עתידיים</ion-title>-->\n<!--</ion-navbar>-->\n\n<!--</ion-header>-->\n\n\n<ion-content padding>\n\n  <back-button></back-button>\n  <div class="mainLogo" align="center">\n    <img src="images/logo.png" style="width:50%; margin: auto">\n  </div>\n  <div class="greenLine"></div>\n\n  <div class="info" align="center" style="margin-top:15px;">\n\n    <div class="MT20" *ngIf="PushNotifications.length == 0">\n      לא נמצאו הודעות\n    </div>\n\n    <div class="mainRow1" *ngFor="let row of PushNotifications let i=index">\n      <div class="rowDiv">\n        <div class="Title1">{{row.title}}</div>\n        <div class="Date1">{{row.date}}</div>\n      </div>\n      <div class="rowDiv">\n        <div class="Hour1">{{row.time}} </div>\n      </div>\n    </div>\n\n\n\n    <!--<ion-list>-->\n      <!--<ion-item *ngFor="let row of PushNotifications let i=index">-->\n        <!--<div style="text-align:right; direction:rtl;">-->\n          <!--<h2>{{row.title}}</h2>-->\n          <!--<p>{{row.date}}</p>-->\n        <!--</div>-->\n      <!--</ion-item>-->\n    <!--</ion-list>-->\n  </div>\n\n\n\n</ion-content>\n\n<ion-footer>\n  <footer></footer>\n</ion-footer>\n\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\pushnotifications\pushnotifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_server_service__["a" /* ServerService */]])
    ], PushnotificationsPage);
    return PushnotificationsPage;
}());

//# sourceMappingURL=pushnotifications.js.map

/***/ })

},[430]);
//# sourceMappingURL=main.js.map