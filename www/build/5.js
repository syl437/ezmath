webpackJsonp([5],{

/***/ 831:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPageModule", function() { return MainPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__main__ = __webpack_require__(842);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(427);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MainPageModule = /** @class */ (function () {
    function MainPageModule() {
    }
    MainPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__main__["a" /* MainPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__main__["a" /* MainPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
            ],
        })
    ], MainPageModule);
    return MainPageModule;
}());

//# sourceMappingURL=main.module.js.map

/***/ }),

/***/ 842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toast_service__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_server_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__classsignup_classsignup__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__previoustickets_previoustickets__ = __webpack_require__(429);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MainPage = /** @class */ (function () {
    function MainPage(navCtrl, navParams, toast, server, platform, auth) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.server = server;
        this.auth = auth;
        this.HomePageDataArray = [];
        //public HomePageDataArray : Array<any>;
        this.FutureLessonArray = [];
        this.DaysWeek = new Array("ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת");
        //private onResumeSubscription: Subscription;
        this.userType = 0;
        this.userCredits = 0;
        this.loading_finshed = false;
        this.userCreditsText = '';
        this.countUnreadMessages = 0;
        this.creditColor = "secondary";
        platform.ready().then(function () {
            document.addEventListener('resume', function () {
                _this.GetHomePageData();
            });
        });
        //     this.onResumeSubscription = this.platform.resume.subscribe(() => {
        //         this.GetHomePageData();
        //     });
        this.userType = localStorage.getItem("userType");
    }
    MainPage.prototype.signUpClassId = function (id) {
        this.navCtrl.push('ClasssignupbyidPage', {
            id: id
        });
    };
    MainPage.prototype.signUpClass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__classsignup_classsignup__["a" /* ClasssignupPage */]);
    };
    MainPage.prototype.futureClassesPage = function () {
        this.navCtrl.push('FutureclassesPage');
    };
    MainPage.prototype.pastClassesPage = function () {
        this.navCtrl.push('PastclassesPage');
    };
    MainPage.prototype.profileEditGo = function () {
        this.navCtrl.push('ProfileeditPage');
    };
    MainPage.prototype.ConfirmClassPage = function () {
        this.navCtrl.push('TeacherconfirmclassPage');
    };
    MainPage.prototype.previousTicketsPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__previoustickets_previoustickets__["a" /* PreviousticketsPage */]);
    };
    MainPage.prototype.userLogOut = function () {
        this.auth.userLogOut('userLogOut', localStorage.getItem("userid"));
        this.navCtrl.setRoot('RegisterIntroPage');
    };
    MainPage.prototype.GetHomePageData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.server.GetHomePageData('GetHomePageData', localStorage.getItem("userid")).then(function (data) {
                            console.log("GetHomePageData : ", data.json());
                            _this.HomePageDataArray = data.json();
                            _this.loading_finshed = true;
                            _this.FutureLessonArray = _this.HomePageDataArray.future_lessons;
                            _this.userCredits = _this.HomePageDataArray.user_group_credits;
                            if (_this.userCredits < 0) {
                                _this.creditColor = "danger";
                                _this.userCreditsText = 'חוב';
                            }
                            else {
                                _this.creditColor = "secondary";
                                _this.userCreditsText = 'ניקובים בכרטיסיה';
                            }
                            _this.countUnreadMessages = _this.HomePageDataArray.unread_messages_count;
                            if (_this.FutureLessonArray.length > 0) {
                                var nextlessondate = new Date(_this.FutureLessonArray[0].class_date);
                                _this.nextlessonday = _this.DaysWeek[nextlessondate.getDay()];
                                _this.nextlessondate = _this.FutureLessonArray[0].class_date.split('-').reverse().join('/');
                                _this.nextlessonhour = _this.FutureLessonArray[0].class_hour;
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MainPage.prototype.ionViewDidLoad = function () {
    };
    MainPage.prototype.ionViewWillEnter = function () {
        this.GetHomePageData();
    };
    MainPage.prototype.ngOnDestroy = function () {
        // always unsubscribe your subscriptions to prevent leaks
        //this.onResumeSubscription.unsubscribe();
    };
    MainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-main',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\main\main.html"*/'<ion-content>\n\n\n\n    <div class="headerLeftIcon" (click)="userLogOut()">\n\n        <ion-icon name="ios-log-out" style="margin-left: 6px;"></ion-icon>\n\n        <div>התנתק</div>\n\n    </div>\n\n\n\n    <div class="headerRightIcon" (click)="ConfirmClassPage()" *ngIf="userType == 2">\n\n        <ion-icon name="ios-grid-outline" style="margin-left: 6px;"></ion-icon>\n\n        <div>אישור שיעור</div>\n\n    </div>\n\n\n\n    <!--<div class="headerRightIcon"  *ngIf="userType == 0">-->\n\n        <!--<ion-badge id="notifications-badge2"  color="secondary"  ><span style="direction: rtl;">ניקובים בכרטיסיה: {{userCredits}}</span></ion-badge>-->\n\n    <!--</div>-->\n\n\n\n    <div class="mainLogo" align="center">\n\n        <img src="images/logo.png" style="width:50%; margin: auto">\n\n    </div>\n\n\n\n    <div class="greenLine"></div>\n\n\n\n    <div class="registerBtn" (click)="signUpClass()">\n\n        <img src="images/home/reg_btn.jpg" width="100%" />\n\n    </div>\n\n\n\n    <div align="center" *ngIf="userType == 0 && loading_finshed" style="margin-top:10px;">\n\n        <ion-badge id="notifications-badge2"  [color]="creditColor"  >\n\n            <div style="direction: ltr;">{{userCredits}}\n\n                <div style="display:inline;">{{userCreditsText}}</div>\n\n            </div>\n\n        </ion-badge>\n\n    </div>\n\n\n\n    <div *ngIf="FutureLessonArray.length > 0">\n\n        <p class="nextLesson"> השיעור הבא : {{nextlessonday}}  {{nextlessondate}} בשעה : {{nextlessonhour}}\n\n        </p>\n\n    </div>\n\n\n\n    <div class="info" align="center">\n\n        <div class="title"></div>\n\n\n\n\n\n        <div class="mainButtons">\n\n            <div class="mainButtonsRows">\n\n                <div class="mainButtonsRowLeft" (click)="profileEditGo()">\n\n                    <img src="images/home/b1.png" style="width: 100%">\n\n                </div>\n\n                <div class="mainButtonsRowRight" (click)="futureClassesPage()">\n\n                    <img src="images/home/b2.png" style="width: 100%">\n\n                </div>\n\n            </div>\n\n            <div class="mainButtonsRows">\n\n                <div class="mainButtonsRowLeft" (click)="previousTicketsPage()">\n\n                    <img src="images/home/b3.png" style="width: 100%">\n\n                </div>\n\n                <div class="mainButtonsRowRight" (click)="pastClassesPage()">\n\n                    <img src="images/home/b4.png" style="width: 100%">\n\n                </div>\n\n            </div>\n\n        </div>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n<ion-footer>\n\n    <footer [messagesCount]="countUnreadMessages"></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\main\main.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_3__services_server_service__["a" /* ServerService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */]])
    ], MainPage);
    return MainPage;
}());

//# sourceMappingURL=main.js.map

/***/ })

});
//# sourceMappingURL=5.js.map