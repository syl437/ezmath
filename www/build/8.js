webpackJsonp([8],{

/***/ 829:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FutureclassesPageModule", function() { return FutureclassesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__futureclasses__ = __webpack_require__(840);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(427);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FutureclassesPageModule = /** @class */ (function () {
    function FutureclassesPageModule() {
    }
    FutureclassesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__futureclasses__["a" /* FutureclassesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__futureclasses__["a" /* FutureclassesPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
            ],
        })
    ], FutureclassesPageModule);
    return FutureclassesPageModule;
}());

//# sourceMappingURL=futureclasses.module.js.map

/***/ }),

/***/ 840:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FutureclassesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_server_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



//import { ComponentsModule } from '../../components/components.module';
///import { BackButtonComponent } from '../../components/back-button/back-button';
/**
 * Generated class for the FutureclassesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FutureclassesPage = /** @class */ (function () {
    function FutureclassesPage(navCtrl, navParams, server, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.server = server;
        this.alertCtrl = alertCtrl;
        this.ClassesArray = [];
        this.ClassStatus = ["חדש", "מאושר", "לא מאושר"];
    }
    FutureclassesPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getFutureClasses();
                return [2 /*return*/];
            });
        });
    };
    FutureclassesPage.prototype.getFutureClasses = function () {
        var _this = this;
        this.server.getFutureClasses('getFutureClasses', localStorage.getItem("userid")).then(function (data) {
            console.log("getFutureClasses : ", data);
            var response = data.json();
            _this.ClassesArray = response;
            console.log("ClassesArray", _this.ClassesArray);
        });
    };
    FutureclassesPage.prototype.getClassStatus = function (status) {
        return this.ClassStatus[status];
    };
    FutureclassesPage.prototype.cancelClass = function (row, index) {
        var _this = this;
        this.server.cancelFutureClass('cancelFutureClass', row.id, localStorage.getItem("userid")).then(function (data) {
            _this.ClassesArray.splice(index, 1);
        });
    };
    FutureclassesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FutureclassesPage');
    };
    FutureclassesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-futureclasses',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\futureclasses\futureclasses.html"*/'<!--\n\n  Generated template for the FutureclassesPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<!--<ion-header>-->\n\n\n\n  <!--<ion-navbar>-->\n\n    <!--<ion-title>שיעורים עתידיים</ion-title>-->\n\n  <!--</ion-navbar>-->\n\n\n\n<!--</ion-header>-->\n\n\n\n\n\n<ion-content padding>\n\n\n\n     <back-button></back-button>\n\n    <div class="mainLogo" align="center">\n\n        <img src="images/logo.png" style="width:50%; margin: auto">\n\n    </div>\n\n    <div class="greenLine"></div>\n\n\n\n    <div class="info" align="center">\n\n\n\n    <div class="MT20" *ngIf="ClassesArray.length == 0">\n\n        לא נמצאו שיעורים עתידיים\n\n    </div>\n\n\n\n        <div class="mainRow1" *ngFor="let row of ClassesArray let i=index">\n\n            <div class="rowDiv">\n\n                <div class="Title1">שם הקבוצה :  {{row.title}}</div>\n\n                <div class="Date1">{{row.class_date}}</div>\n\n            </div>\n\n            <div class="rowDiv">\n\n                <div class="Status1" *ngIf="row.approve_status != 2">סטטוס :  {{getClassStatus(row.approve_status)}}</div>\n\n                <div class="Status1" *ngIf="row.approve_status == 2"> סטטוס: <span class="NotApproved">{{getClassStatus(row.approve_status)}}</span></div>                <div class="Hour1"> שעה :  {{row.class_hour}} </div>\n\n                 <button ion-button [disabled]="!row.can_delete" color="secondary"  type="button"   class = "MT20 regButton" (click)="cancelClass(row,i)">מחק שיעור</button>\n\n                <!--<div>{{row.can_delete}}</div>-->\n\n                <!--<div>{{row.hours_passed}}</div>-->\n\n            </div>\n\n        </div>\n\n\n\n  </div>\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n\n\n<ion-footer>\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\futureclasses\futureclasses.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_server_service__["a" /* ServerService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], FutureclassesPage);
    return FutureclassesPage;
}());

//# sourceMappingURL=futureclasses.js.map

/***/ })

});
//# sourceMappingURL=8.js.map