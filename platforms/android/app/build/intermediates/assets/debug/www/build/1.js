webpackJsonp([1],{

/***/ 838:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__(847);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]),
            ],
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());

//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 847:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_toast_service__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_server_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_facebook__ = __webpack_require__(173);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, auth, Toast, server, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.Toast = Toast;
        this.server = server;
        this.fb = fb;
        this.RegisterStep = 1;
        this.info = {
            student_name: '',
            phone: '',
            email: '',
            password: '',
            city: '',
            schoolgrade: '',
            teachinglevel: '',
            branch_id: '',
            scool_name: '',
            parent_name: '',
            parent_phone: '',
        };
        this.schoolgradeArray = [];
        this.teachinglevelArray = [];
        this.branchesArray = [];
        this.isLoggedIn = false;
    }
    RegisterPage.prototype.getSchoolGrade = function () {
        var _this = this;
        this.server.GetData('webGetSchoolGrade').then(function (data) {
            _this.schoolgradeArray = data.json();
            console.log("webGetSchoolGrade : ", data.json());
        });
    };
    RegisterPage.prototype.getTeachingLevel = function () {
        var _this = this;
        this.server.GetData('webGetTeachingLevel').then(function (data) {
            console.log("webGetTeachingLevel : ", data.json());
            _this.teachinglevelArray = data.json();
        });
    };
    RegisterPage.prototype.getBranches = function () {
        var _this = this;
        this.server.GetData('GetBranches').then(function (data) {
            console.log("GetBranches : ", data.json());
            _this.branchesArray = data.json();
        });
    };
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getSchoolGrade();
                this.getTeachingLevel();
                this.getBranches();
                return [2 /*return*/];
            });
        });
    };
    RegisterPage.prototype.faceBookConnect = function () {
        var _this = this;
        this.fb.login(['public_profile', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                //this.isLoggedIn = true;
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                //this.isLoggedIn = false;
                //alert(JSON.stringify(res));
            }
        })
            .catch(function (e) {
            return console.log(e);
        });
    };
    RegisterPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.info.email = res['email'];
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    RegisterPage.prototype.registerForm = function () {
        var _this = this;
        //let fi = this.fileInput.nativeElement;
        var fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.RegisterStep == 1) {
            if (!this.info['student_name'])
                this.Toast.presentToast('הכנס שם התלמיד');
            else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
                this.Toast.presentToast('הכנס מספר טלפון חוקי');
            else if (this.info['email'].length == 0)
                this.Toast.presentToast('הכנס כתובת מייל');
            else if (!this.emailregex.test(this.info['email'])) {
                this.info['email'] = '';
                this.Toast.presentToast('מייל לא תקין');
            }
            else if (this.info['password'].length == 0)
                this.Toast.presentToast('הכנס סיסמה');
            else if (this.info['city'].length == 0)
                this.Toast.presentToast('הכנס עיר');
            else {
                this.RegisterStep = 2;
            }
        }
        else if (this.RegisterStep == 2) {
            if (!this.info['schoolgrade'])
                this.Toast.presentToast('יש לבחור כיתה');
            else if (!this.info['teachinglevel'])
                this.Toast.presentToast('יש לבחור רמת לימוד');
            else if (!this.info['branch_id'])
                this.Toast.presentToast('יש לבחור סניף');
            else if (this.info['scool_name'].length == 0)
                this.Toast.presentToast('הכנס שם בית ספר');
            else if (this.info['parent_name'].length == 0 && this.info['schoolgrade'] != 14)
                this.Toast.presentToast('הכנס שם הורה');
            else if ((this.info['parent_phone'].length < 8 || this.info['parent_phone'].length > 10) && this.info['schoolgrade'] != 14)
                this.Toast.presentToast('הכנס טלפון הורה חוקי');
            else {
                this.auth.registerUser('RegisterUser', this.info, fileToUpload, localStorage.getItem("push_id")).then(function (data) {
                    console.log("registerUser : ", data);
                    var response = data.json();
                    if (response.length > 0) {
                        localStorage.setItem("userid", response[0].id);
                        localStorage.setItem("userType", "0");
                        _this.navCtrl.setRoot('MainPage');
                        _this.RegisterStep = 1;
                    }
                    else {
                        _this.RegisterStep = 1;
                        _this.Toast.presentToast('אימייל כבר בשימוש יש להזין מייל אחר');
                    }
                });
            }
        }
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\ezmath\src\pages\register\register.html"*/'\n\n\n\n<ion-content class="Content" >\n\n    <div class="mainLogo" align="center">\n\n        <img src="images/logo.png" style="width:50%; margin: auto">\n\n    </div>\n\n    <div class="info" align="center">\n\n        <div class="titleImg">\n\n            <img src="images/new_user.png" style="width: 100%" />\n\n        </div>\n\n\n\n           <div id="RegisterForm" *ngIf="RegisterStep == 1" class="MT20">\n\n               <!--\n\n               <div class="form-group">\n\n                   <input type="text" class="form-control textWhite"  name="username" ngModel required  placeholder="* הכנס שם משתמש"><br>\n\n               </div>\n\n                -->\n\n               <div class="form-group">\n\n                   <input type="text" class="form-control textWhite" [(ngModel)]="info.student_name"   required  placeholder="הכנס שם תלמיד"><br>\n\n               </div>\n\n\n\n               <div class="form-group">\n\n                   <input type="tel" class="form-control textWhite"  [(ngModel)]="info.phone"   required  placeholder="הכנס טלפון"><br>\n\n               </div>\n\n\n\n               <div class="form-group">\n\n                   <input type="email" class="form-control textWhite" [(ngModel)]="info.email"   required  placeholder="* הכנס אימייל"><br>\n\n               </div>\n\n\n\n\n\n               <div class="form-group">\n\n                   <input type="text" class="form-control textWhite"  [(ngModel)]="info.password" required  placeholder="* הכנס סיסמה"><br>\n\n               </div>\n\n\n\n               <div class="form-group">\n\n                   <input type="text" class="form-control textWhite" [(ngModel)]="info.city"   required  placeholder="הכנס עיר"><br>\n\n               </div>\n\n\n\n               <button ion-button class="regButton" color="secondary"  type="button" (click)="registerForm()">הבא</button>\n\n\n\n               <div style="width:80%; margin: auto">\n\n                   <div  (click)="faceBookConnect()">\n\n                       <img src="images/main_bt3.png" style="width: 100%" class="MT20" />\n\n                   </div>\n\n               </div>\n\n\n\n            </div>\n\n\n\n            <div id="RegisterForm2" *ngIf="RegisterStep == 2">\n\n\n\n\n\n                <div class="form-group">\n\n\n\n                    <select id="schoolgrade" class="form-control"  [(ngModel)]="info.schoolgrade"  required >\n\n                        <option value="" selected>בחירת כיתה</option>\n\n                        <option *ngFor="let item of schoolgradeArray let i=index" [value]="item.id" >{{item.title}}</option>\n\n                    </select>\n\n                </div>\n\n\n\n                <div class="form-group" >\n\n                        <select id="teachinglevel" class="form-control" [(ngModel)]="info.teachinglevel"   required >\n\n                        <option value="" selected>בחירת רמת לימוד</option>\n\n                            <option *ngFor="let item of teachinglevelArray let i=index" [value]="item.id" >{{item.title}}</option>\n\n                    </select>\n\n                </div>\n\n\n\n\n\n                <div class="form-group" >\n\n                    <select id="branch_id" class="form-control" [(ngModel)]="info.branch_id"   required >\n\n                        <option value="" selected>בחירת סניף</option>\n\n                        <option *ngFor="let item of branchesArray let i=index" [value]="item.id" >{{item.title}}</option>\n\n                    </select>\n\n                </div>\n\n\n\n                <div class="form-group">\n\n                    <input type="text" class="form-control textWhite" [(ngModel)]="info.scool_name"   required  placeholder="* הכנס שם בית ספר"><br>\n\n                </div>\n\n\n\n                <div class="form-group">\n\n                    <input type="text" class="form-control textWhite" [(ngModel)]="info.parent_name"   required  placeholder="* הכנס שם הורה"><br>\n\n                </div>\n\n\n\n                <div class="form-group">\n\n                    <input type="tel" class="form-control textWhite" [(ngModel)]="info.parent_phone"   required  placeholder="* הכנס טלפון הורה"><br>\n\n                </div>\n\n\n\n                <button ion-button class="regButton" color="secondary"   type="button" (click)="registerForm()">אישור</button>\n\n            </div>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\ezmath\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_4__services_server_service__["a" /* ServerService */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_facebook__["a" /* Facebook */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

});
//# sourceMappingURL=1.js.map