import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup, Validators,NgForm} from "@angular/forms";
import {AuthService} from "../../services/auth-service";
import {ToastService} from "../../services/toast-service";
import {ServerService} from "../../services/server-service";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage implements OnInit {

    //registerForm: FormGroup;
    public emailregex;
    RegisterStep:number = 1;
    public info: any = {
        student_name: '',
        phone: '',
        email: '',
        password: '',
        city: '',
        schoolgrade: '',
        teachinglevel:'',
        branch_id: '',
        scool_name: '',
        parent_name: '',
        parent_phone: '',
    };


    public schoolgradeArray : any = [];
    public teachinglevelArray : any = [];
    public branchesArray : any = [];
    public isLoggedIn:boolean = false;


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public auth:AuthService,
                public Toast:ToastService, public server: ServerService, private fb: Facebook) {
    }

    getSchoolGrade() {

        this.server.GetData('webGetSchoolGrade').then((data: any) => {
            this.schoolgradeArray = data.json();
            console.log("webGetSchoolGrade : " , data.json());
        });
    }

    getTeachingLevel() {
        this.server.GetData('webGetTeachingLevel').then((data: any) => {
            console.log("webGetTeachingLevel : " , data.json());
            this.teachinglevelArray = data.json();
        });
    }

    getBranches() {
        this.server.GetData('GetBranches').then((data: any) => {
            console.log("GetBranches : " , data.json());
            this.branchesArray = data.json();
        });
    }



    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

    async ngOnInit() {
        this.getSchoolGrade();
        this.getTeachingLevel();
        this.getBranches();
    }

    faceBookConnect() {
        this.fb.login(['public_profile', 'email'])
            .then(res => {
                if(res.status === "connected") {
                    //this.isLoggedIn = true;
                    this.getUserDetail(res.authResponse.userID);
                } else {
                    //this.isLoggedIn = false;
                    //alert(JSON.stringify(res));
                }
            })
            .catch(e =>
                console.log(e)
            );
    }


    getUserDetail(userid)
    {
        this.fb.api("/"+userid+"/?fields=id,email,name,gender",["public_profile"])
            .then(res => {
                console.log(res);
                this.info.email = res['email'];
            })
            .catch(e => {
                console.log(e);
            });
    }


    registerForm()
    {


        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}
        this.emailregex = /\S+@\S+\.\S+/;



        if (this.RegisterStep == 1)
        {
            if (!this.info['student_name'])
                this.Toast.presentToast('הכנס שם התלמיד');

            else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
                this.Toast.presentToast('הכנס מספר טלפון חוקי');

            else if (this.info['email'].length == 0)
                this.Toast.presentToast('הכנס כתובת מייל');

            else if (!this.emailregex.test(this.info['email'])) {
                this.info['email'] = '';
                this.Toast.presentToast('מייל לא תקין');
            }

            else if (this.info['password'].length == 0)
                this.Toast.presentToast('הכנס סיסמה');

            else if (this.info['city'].length == 0)
                this.Toast.presentToast('הכנס עיר');

            else {
                this.RegisterStep = 2;
            }
        }


              else if (this.RegisterStep == 2)
             {

                if (!this.info['schoolgrade'])
                        this.Toast.presentToast('יש לבחור כיתה');

                else if (!this.info['teachinglevel'])
                    this.Toast.presentToast('יש לבחור רמת לימוד');

                else if (!this.info['branch_id'])
                    this.Toast.presentToast('יש לבחור סניף');

                else if (this.info['scool_name'].length == 0)
                    this.Toast.presentToast('הכנס שם בית ספר');

                else if (this.info['parent_name'].length == 0 && this.info['schoolgrade'] != 14)
                    this.Toast.presentToast('הכנס שם הורה');

                else if ((this.info['parent_phone'].length < 8 || this.info['parent_phone'].length > 10) && this.info['schoolgrade'] != 14)
                    this.Toast.presentToast('הכנס טלפון הורה חוקי');

                else
                {
                    this.auth.registerUser('RegisterUser',this.info,fileToUpload,localStorage.getItem("push_id")).then((data: any) => {
                        console.log("registerUser : " , data);
                        let response = data.json()
                        if (response.length > 0)
                        {
                            localStorage.setItem("userid", response[0].id);
                            localStorage.setItem("userType","0");
                            this.navCtrl.setRoot('MainPage');
                            this.RegisterStep = 1;
                        }
                        else
                        {
                            this.RegisterStep = 1;
                            this.Toast.presentToast('אימייל כבר בשימוש יש להזין מייל אחר');
                        }
                    });
                }
        }
    }
}
