import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import moment from 'moment';
import {ServerService} from "../../services/server-service";
import {ToastService} from "../../services/toast-service";

/**
 * Generated class for the TeacherconfirmclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teacherconfirmclass',
  templateUrl: 'teacherconfirmclass.html',
})
export class TeacherconfirmclassPage implements OnInit {

    public todaydate :any  = moment().format('YYYY-MM-DD');

    public info:any = {
        'branchselect' : '',
        'professionselect' : '',
        'class_date' : this.todaydate,
    }

    public senddetails:any = {
        'user_id' : '',
        'class_id' : '',
        'approve_status' : '',
        'payment_method' : '',
    }


    public brancesArray:any;
    public professionArray:any;
    public searchArray:any = [];
    public showFieldsdiv:boolean = true;


    constructor(public navCtrl: NavController, public navParams: NavParams, public server: ServerService,
                public Toast:ToastService,private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeacherconfirmclassPage');
  }

    async ngOnInit() {
        this.getBranches();
        //this.GetProfessions();
    }

    getBranches() {
        this.server.GetBranches('GetBranches').then((data: any) => {
            console.log("GetBranches : " , data.json());
            this.brancesArray = data.json();
        });
    }

    GetProfessions() {
        this.server.GetBranches('GetProfessions').then((data: any) => {
            console.log("GetProfessions : " , data.json());
            this.professionArray = data.json();
        });
    }


    confirmClass(row,type) {
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.approve_status = type;
        this.server.SearchClasses('ApproveClassesManager',this.senddetails,localStorage.getItem("userid")).then((data: any) => {
            console.log("ApproveClassesManager : " , data.json());
            row.confirmed_status = type;
        });
    }

    didntShowClass(row,type)
    {

        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;

        this.server.userDidntShow('userDidntShow',this.senddetails,localStorage.getItem("userid")).then((data: any) => {
            console.log("userDidntShow : " , data.json());
            let dataReponse = data.json();
            let alertResponse = '';
            if (type == 0)
                row.didnt_show = 1;
            else
                row.didnt_show = 0;
        });
    }
    searchClass() {

          // if (!this.info.professionselect)
          //   this.Toast.presentToast('יש לבחור מקצוע');


        if (!this.info.branchselect)
            this.Toast.presentToast('יש לבחור סניף');

        else if (!this.info.class_date)
            this.Toast.presentToast('יש לבחור תאריך');

        else {
          this.server.SearchClasses('SearchClassesManager',this.info,localStorage.getItem("userid")).then((data: any) => {
              console.log("SearchClassesManager1111 : " , data.json());
              this.searchArray = data.json();

              if (data.json() == 0) {
                  this.Toast.presentToast('לא נמצאו תוצאות');
                  this.searchArray = [];
              } else{
                  this.showFieldsdiv = false;
              }
          });
        }
    }

    showFieldsSearch() {
        this.showFieldsdiv = true;
    }

    paymentCash(row) {

        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 0;

        let alertConfirm = this.alertCtrl.create({
            title: 'אישור רכישה',
            message: 'האם לאשר רכישה במזומן?',
            buttons: [

                {
                    text: 'אישור',
                    handler: () => {


                        this.server.ClassPayment('ClassCashPayment',this.senddetails,localStorage.getItem("userid")).then((data: any) => {
                            console.log("ClassCashPayment : " , data.json());
                            let dataReponse = data.json();
                            let alertResponse = '';

                            switch(dataReponse) {
                                case 0:
                                    alertResponse = 'רכישה במזומן בוצעה בהצלחה';
                                    break;
                                case 1:
                                    alertResponse = 'תשלום כבר בוצעה';
                                    break;
                                default:
                                    alertResponse = '';
                            }

                            let okAlert = this.alertCtrl.create({
                                title: alertResponse,
                                buttons: ['אישור']
                            });
                            okAlert.present();

                            row.paid_count = 1;
                            row.paid_method = 0;
                        });
                    }
                },
                {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }

            ]
        });
        alertConfirm.present();

    }

    paymentTicket(row) {
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 1;

        let alertConfirm = this.alertCtrl.create({
            title: 'אישור רכישה',
            message: 'האם לאשר רכישה בכרטיסיה?',
            buttons: [

                {
                    text: 'אישור',
                    handler: () => {


                        this.server.ClassPayment('ClassTicketPaymentConfirm',this.senddetails,localStorage.getItem("userid")).then((data: any) => {
                            console.log("ClassTicketPaymentConfirm : " , data.json());
                            let dataReponse = data.json();
                            let alertResponse = '';

                            switch(dataReponse) {
                                case 0:
                                    alertResponse = 'רכישה בכרטיסיה בוצעה בהצלחה';
                                    break;
                                case 1:
                                    alertResponse = 'תשלום כבר בוצעה';
                                    break;
                                case 2:
                                    alertResponse = 'למשתמש זה לא שויך קבוצת כרטסיות , יש לפתוח קבוצה במערכת ניהול';
                                    break;
                                case 3:
                                    alertResponse = 'לא נותרה יתרה בכרטיסיה של המשתמש';
                                    break;
                                default:
                                    alertResponse = '';
                            }

                            if (dataReponse == 0 || dataReponse == 1 || dataReponse == 2) {
                                let okAlert = this.alertCtrl.create({
                                    title: alertResponse,
                                    buttons: ['אישור']
                                });
                                okAlert.present();

                                if (dataReponse == 0 || dataReponse == 1)
                                {
                                    row.paid_count = 1;
                                    row.paid_method = 1;
                                }

                            }
                            else {
                                let Confirmalert2 = this.alertCtrl.create({
                                    title: 'לא נותרה יתרה בכרטיסיה של המשתמש',
                                    buttons: [
                                        {
                                            text: 'קניית כרטיסיה ותשלום',
                                            handler: () => {
                                                this.server.ClassPayment('ClassCashPaymentTicketBuy',this.senddetails,localStorage.getItem("userid")).then((data: any) => {
                                                    console.log("ClassCashPaymentTicketBuy : " , data.json());
                                                    let dataReponse = data.json();
                                                    let okAlert = this.alertCtrl.create({
                                                        title: "קניית כרטיסיה ותשלום בוצע בהצלחה",
                                                        buttons: ['אישור']
                                                    });
                                                    okAlert.present();
                                                });

                                                row.paid_count = 1;
                                                row.paid_method = 1;
                                            }
                                        },
                                        {
                                            text: 'תשלום בחוב',
                                            handler: () => {
                                                this.server.ClassPayment('ClassCashPaymentDebt',this.senddetails,localStorage.getItem("userid")).then((data: any) => {
                                                    console.log("ClassCashPaymentDebt : " , data.json());
                                                    let dataReponse = data.json();
                                                    let okAlert = this.alertCtrl.create({
                                                        title: "תשלום בחוב בוצע בהצלחה",
                                                        buttons: ['אישור']
                                                    });
                                                    okAlert.present();
                                                });
                                                row.paid_count = 1;
                                                row.paid_method = 1;
                                            }
                                        }  ,
                                        {
                                            text: 'ביטול',
                                            role: 'cancel',
                                            handler: () => {
                                                console.log('Cancel clicked');
                                            }
                                        },
                                    ]
                                });
                                Confirmalert2.present();
                            }
                        });
                    }
                },
                {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alertConfirm.present();
    }

    paymentDebt(row)
    {
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 2;

        let alertConfirm = this.alertCtrl.create({
            title: 'אישור רכישה',
            message: 'האם לאשר רכישה בחוב?',
            buttons: [

                {
                    text: 'אישור',
                    handler: () => {

                        this.server.ClassPayment('ClassDebtPayment',this.senddetails,localStorage.getItem("userid")).then((data: any) => {
                            console.log("ClassCashPayment : " , data.json());
                            let dataReponse = data.json();
                            let alertResponse = '';

                            switch(dataReponse) {
                                case 0:
                                    alertResponse = 'רכישה בחוב בוצעה בהצלחה';
                                    break;
                                case 1:
                                    alertResponse = 'תשלום כבר בוצעה';
                                    break;
                                default:
                                    alertResponse = '';
                            }

                            let okAlert = this.alertCtrl.create({
                                title: alertResponse,
                                buttons: ['אישור']
                            });
                            okAlert.present();

                            row.paid_count = 1;
                            row.paid_method = 2;
                        });
                    }
                },
                {
                    text: 'ביטול',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alertConfirm.present();
    }



    backButtonClick() {
        this.navCtrl.pop();
    }


}
