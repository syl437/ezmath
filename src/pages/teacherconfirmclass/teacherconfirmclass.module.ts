import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeacherconfirmclassPage } from './teacherconfirmclass';
import {ComponentsModule} from "../../components/components.module";


@NgModule({
  declarations: [
    TeacherconfirmclassPage,
  ],
  imports: [
    IonicPageModule.forChild(TeacherconfirmclassPage),
      //ComponentsModule,
  ],
})
export class TeacherconfirmclassPageModule {}
