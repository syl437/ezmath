import { Component ,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppSettings } from '../../services/app-settings';
import {ToastService} from "../../services/toast-service";
import {ServerService} from "../../services/server-service";

import { Subscription } from 'rxjs';
import { Platform } from 'ionic-angular';
import {AuthService} from "../../services/auth-service";
import {ClasssignupPage} from "../classsignup/classsignup";
import {TeacherconfirmclassPage} from "../teacherconfirmclass/teacherconfirmclass";
import {PreviousticketsPage} from "../previoustickets/previoustickets";

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage implements OnInit {

    public HomePageDataArray:any = [];
    //public HomePageDataArray : Array<any>;
    public FutureLessonArray:any = [];

    public DaysWeek = new Array(
        "ראשון",
        "שני",
        "שלישי",
        "רביעי",
        "חמישי",
        "שישי",
        "שבת",
    );

    public nextlessonday:any;
    public nextlessondate:any;
    public nextlessonhour:any;
    //private onResumeSubscription: Subscription;
    public userType : any = 0;
    public userCredits : any = 0;
    public loading_finshed:boolean = false;
    public userCreditsText : any = '';
    public countUnreadMessages : any = 0;
    public creditColor = "secondary";

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public toast:ToastService, public server: ServerService,platform: Platform,public auth:AuthService) {

      platform.ready().then(() =>
      {
          document.addEventListener('resume', () => {
              this.GetHomePageData();
          });
      });


      //     this.onResumeSubscription = this.platform.resume.subscribe(() => {
      //         this.GetHomePageData();
      //     });


      this.userType = localStorage.getItem("userType");
  }

    signUpClassId(id) {
        this.navCtrl.push('ClasssignupbyidPage', {
            id: id
        });
    }

  signUpClass()
  {
      this.navCtrl.push(ClasssignupPage);
  }

  futureClassesPage()
  {
      this.navCtrl.push('FutureclassesPage');
  }

  pastClassesPage()
  {
    this.navCtrl.push('PastclassesPage');
  }

 profileEditGo()
 {
     this.navCtrl.push('ProfileeditPage');
 }

    ConfirmClassPage() {
        this.navCtrl.push('TeacherconfirmclassPage');
    }

    previousTicketsPage() {
        this.navCtrl.push(PreviousticketsPage);
    }



  userLogOut()
  {
      this.auth.userLogOut('userLogOut',localStorage.getItem("userid"));
      this.navCtrl.setRoot('RegisterIntroPage');
  }


    async GetHomePageData()
    {
        await this.server.GetHomePageData('GetHomePageData',localStorage.getItem("userid")).then((data: any) => {
            console.log("GetHomePageData : " , data.json());

            this.HomePageDataArray = data.json();
            this.loading_finshed = true;

           this.FutureLessonArray = this.HomePageDataArray.future_lessons;
            this.userCredits = this.HomePageDataArray.user_group_credits;
            if (this.userCredits < 0) {
                this.creditColor = "danger";
                this.userCreditsText = 'חוב';
            }
            else {
                this.creditColor = "secondary";
                this.userCreditsText = 'ניקובים בכרטיסיה';

            }

            this.countUnreadMessages = this.HomePageDataArray.unread_messages_count;


            if (this.FutureLessonArray.length > 0)
            {
                let nextlessondate = new Date(this.FutureLessonArray[0].class_date);
                this.nextlessonday = this.DaysWeek[nextlessondate.getDay()];
                this.nextlessondate = this.FutureLessonArray[0].class_date.split('-').reverse().join('/');
                this.nextlessonhour = this.FutureLessonArray[0].class_hour;
            }

        });

    }

    async ngOnInit() {
        //this.GetHomePageData();
    }

  ionViewDidLoad() {

  }

    ionViewWillEnter(){
        this.GetHomePageData();
    }


        ngOnDestroy() {
        // always unsubscribe your subscriptions to prevent leaks
        //this.onResumeSubscription.unsubscribe();
    }


}
