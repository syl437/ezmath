import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController } from 'ionic-angular';
import {ServerService} from "../../services/server-service";
//import { ComponentsModule } from '../../components/components.module';
///import { BackButtonComponent } from '../../components/back-button/back-button';


/**
 * Generated class for the FutureclassesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-futureclasses',
  templateUrl: 'futureclasses.html',
})
export class FutureclassesPage implements OnInit {

  public ClassesArray:any = [];
  public ClassStatus :any =  ["חדש","מאושר","לא מאושר"];

    constructor(public navCtrl: NavController, public navParams: NavParams,public server:ServerService,private alertCtrl: AlertController) {
  }

  async ngOnInit() {
    this.getFutureClasses();
  }

  getFutureClasses()
  {
      this.server.getFutureClasses('getFutureClasses',localStorage.getItem("userid")).then((data: any) => {
          console.log("getFutureClasses : " , data);
          let response = data.json();
          this.ClassesArray = response;
          console.log("ClassesArray",this.ClassesArray)
      });
  }

    getClassStatus(status) {
        return this.ClassStatus[status];
    }

    cancelClass(row,index) {

        this.server.cancelFutureClass('cancelFutureClass',row.id,localStorage.getItem("userid")).then((data: any) => {
            this.ClassesArray.splice(index, 1);
        });
    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad FutureclassesPage');
  }

}
