import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServerService} from "../../services/server-service";

/**
 * Generated class for the PushnotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pushnotifications',
  templateUrl: 'pushnotifications.html',
})
export class PushnotificationsPage implements OnInit {
  public PushNotifications :any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public server:ServerService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PushnotificationsPage');
  }

    getPushNotifications()
    {
        this.server.getPushNotifications('getPushNotifications',localStorage.getItem("userid")).then((data: any) => {
            console.log("getPushNotifications : " , data);
            let response = data.json();
            this.PushNotifications = response;
            console.log("getPushNotifications",this.PushNotifications)
        });
    }

    async ngOnInit() {
    //this.getPushNotifications();
    }


    ionViewWillEnter(){
        this.getPushNotifications();
    }

}
