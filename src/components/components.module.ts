import { NgModule } from '@angular/core';
import { FooterComponent } from './footer/footer';
import { IonicModule } from 'ionic-angular';
import { FacebookconnectComponent } from './facebookconnect/facebookconnect';
import { BackButtonComponent } from './back-button/back-button';

@NgModule({
	declarations: [FooterComponent,
    FacebookconnectComponent,
    BackButtonComponent,
    ],
	imports: [IonicModule.forRoot(FooterComponent)],
	exports: [FooterComponent,
    FacebookconnectComponent,
    BackButtonComponent,
    ]
})
export class ComponentsModule {}
