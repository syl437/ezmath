import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {LoadingController} from "ionic-angular";
import { AppSettings } from './app-settings'
import {Storage} from '@ionic/storage';
import 'rxjs/Rx';

import {Http,Headers,Response, RequestOptions} from "@angular/http";
import { HttpModule } from '@angular/http';


@Injectable()
export class AuthService {

    constructor(public loadingCtrl: LoadingController,
                public storage: Storage,
                private http: Http) {
    }


    UserLogin(url,data,push_id) {

        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("push_id", push_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise() ; // Reach here if fails
        } catch (err) {
            console.log( "error11:" , err);
        } finally {
            loading.dismiss();
        }
    }

    registerUser(url,data,File,push_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("push_id", push_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    faceBookLogin(url,data) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", data);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }


    SetUserPush(url,push_token,user_id)
    {
        if (push_token)
            localStorage.setItem("push_id", push_token);

        if (user_id && push_token)
        {
            try {
                let body = new FormData();
                body.append("user_id", user_id);
                body.append("push_id", push_token);
                return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
                }).toPromise();
            } catch (err) {
                console.log( err);
            } finally {
            }
        }

    }

    userLogOut(url,user_id)
    {
        localStorage.removeItem("userid");
        localStorage.removeItem("userType");
    }

}

//this._categories.next(categories);
//resolve(this.categories);